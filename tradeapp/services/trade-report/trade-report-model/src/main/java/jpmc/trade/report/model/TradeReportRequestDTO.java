package jpmc.trade.report.model;


import java.util.Date;
/**
 * @Auther Prashant Goyal
 * TradeReportRequestDTO used to hold instruction information.
 */
public class TradeReportRequestDTO  {
	
	
	private String entity;
	private String tradeIndicator;
	private double agreedFx;
	private String currency;
	private Date instructionDate;
	private Date settlementDate;
	private int units;
	private double pricePerUnit;
	
	
	
	public TradeReportRequestDTO(){
		
	}
	/**
	 * @return the entity
	 */
	public String getEntity() {
		return entity;
	}
	/**
	 * @param entity the entity to set
	 */
	public void setEntity(String entity) {
		this.entity = entity;
	}
	/**
	 * @return the tradeIndicator
	 */
	public String getTradeIndicator() {
		return tradeIndicator;
	}
	/**
	 * @param tradeIndicator the tradeIndicator to set
	 */
	public void setTradeIndicator(String tradeIndicator) {
		this.tradeIndicator = tradeIndicator;
	}
	/**
	 * @return the agreedFx
	 */
	public double getAgreedFx() {
		return agreedFx;
	}
	/**
	 * @param agreedFx the agreedFx to set
	 */
	public void setAgreedFx(double agreedFx) {
		this.agreedFx = agreedFx;
	}
	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}
	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	/**
	 * @return the instructionDate
	 */
	public Date getInstructionDate() {
		return instructionDate;
	}
	/**
	 * @param instructionDate the instructionDate to set
	 */
	public void setInstructionDate(Date instructionDate) {
		this.instructionDate = instructionDate;
	}
	/**
	 * @return the settlementDate
	 */
	public Date getSettlementDate() {
		return settlementDate;
	}
	/**
	 * @param settlementDate the settlementDate to set
	 */
	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}
	/**
	 * @return the units
	 */
	public int getUnits() {
		return units;
	}
	/**
	 * @param units the units to set
	 */
	public void setUnits(int units) {
		this.units = units;
	}
	/**
	 * @return the pricePerUnit
	 */
	public double getPricePerUnit() {
		return pricePerUnit;
	}
	/**
	 * @param pricePerUnit the pricePerUnit to set
	 */
	public void setPricePerUnit(double pricePerUnit) {
		this.pricePerUnit = pricePerUnit;
	}
	/**
	 * @return String 
	 */
	@Override
	public String toString(){
		 return "Entity Name :"+this.entity+",Trade Indicator :"+this.tradeIndicator+",AgreedFx :"+this.agreedFx+",Currency Name :"+this.currency+",Instruction Date :"+this.instructionDate+",Settlement Date :"+this.settlementDate+",Units :"+this.units+",Price Per Unit :"+this.pricePerUnit;
		
		
	}
	
	
	
}
