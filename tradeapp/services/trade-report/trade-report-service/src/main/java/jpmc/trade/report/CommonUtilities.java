package jpmc.trade.report;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;



import org.apache.log4j.Logger;

import jpmc.trade.report.model.TradeReportRequestDTO;

public class CommonUtilities {
	
	private static Logger log = Logger.getLogger(CommonUtilities.class);

	
	public static void main(String[] arg) throws IOException {
		String fileName="E:\\software\\edinburgh\\workspace_jpmc\\tradeapp\\services\\trade-report\\trade-report-service\\src\\main\\resources\\inst.txt";
		CommonUtilities d=new CommonUtilities();
		d.readInstructionFile(fileName);
	}
	public ArrayList<TradeReportRequestDTO> readInstructionFile(String fileName) throws IOException {
		
	    BufferedReader br = new BufferedReader(new FileReader(fileName));
	    ArrayList<TradeReportRequestDTO> TradeReportRequestDTOList = new ArrayList<TradeReportRequestDTO>();
	    String newLine;
	    log.info("Trade instructions:");
	    while ((newLine = br.readLine()) != null) {
	    	
	        log.info(newLine);
	        String [] TradeReportRequestData=newLine.split(",");
	        TradeReportRequestDTO tradeReportRequestDTO=new TradeReportRequestDTO();
	        tradeReportRequestDTO.setEntity(TradeReportRequestData[0]);
	        tradeReportRequestDTO.setTradeIndicator(TradeReportRequestData[1]);
	        tradeReportRequestDTO.setAgreedFx(Double.parseDouble(TradeReportRequestData[2]));
	        tradeReportRequestDTO.setCurrency(TradeReportRequestData[3]);
	        tradeReportRequestDTO.setInstructionDate(convertFromStringToDate(TradeReportRequestData[4]));
	        tradeReportRequestDTO.setSettlementDate(convertFromStringToDate(TradeReportRequestData[5]));
	        tradeReportRequestDTO.setUnits(Integer.parseInt(TradeReportRequestData[6]));
	        String data=TradeReportRequestData[7];
	        tradeReportRequestDTO.setPricePerUnit(Double.parseDouble(data));
	        
	        TradeReportRequestDTOList.add(tradeReportRequestDTO);
	    }
	    br.close();
	    return TradeReportRequestDTOList;
	}
	
	private Date convertFromStringToDate(String date)  {
		DateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
		Date utilDate=null;
		try{
		 utilDate = formatter.parse(date);
		}catch(ParseException pe){pe.printStackTrace();}
		return utilDate;
		
	}

}
