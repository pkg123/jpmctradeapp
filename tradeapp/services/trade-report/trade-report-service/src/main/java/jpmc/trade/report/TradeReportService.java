package jpmc.trade.report;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import jpmc.trade.report.model.TradeReportRequestDTO;

/**
 * 
 * @author Prashant Goyal
 * class  TradeReportService.
 *
 */
public class TradeReportService {

	CommonUtilities utilities = new CommonUtilities();
	Double buyAmount = 0.0;
	Double sellAmoint = 0.0;
	HashMap<String, Double> currencyMap = new HashMap<String, Double>();
	HashMap<String, Double> currencyBuyMap = new HashMap<String, Double>();
	HashMap<String, Double> currencySellMap = new HashMap<String, Double>();

	private static Logger log = Logger.getLogger(TradeReportApi.class);

	public static void main(String[] arg) throws IOException {
		String fileName="E:\\software\\edinburgh\\workspace_jpmc\\tradeapp\\services\\trade-report\\trade-report-service\\src\\main\\resources\\inst.txt";
		TradeReportService ds = new TradeReportService();
		ds.printTradeReport(fileName);
		ds.printRank();
	}

	/**
	 * TradeReportConfiguration
	 * 
	 * @return the Trade Report Service
	 */
	private void printRank() {

		Map.Entry<String, Double> maxBuyEntry = currencyBuyMap.entrySet().stream().max(Map.Entry.comparingByValue())
				.get();

		Map.Entry<String, Double> maxSellEntry = currencySellMap.entrySet().stream().max(Map.Entry.comparingByValue())
				.get();

		log.info("Total outgoing amount: " + buyAmount);
		log.info("Total incoming amount: " + sellAmoint);
		log.info("Max Buy Entry for currencyy " + maxBuyEntry.getKey() + " is  " + maxBuyEntry.getValue());
		log.info("Max Sell Entry for currencyy " + maxSellEntry.getKey() + " is  " + maxSellEntry.getValue());

	}
	
	/**
	 * printTradeReport
	 * 
	 * @param String fileName
	 */
	@SuppressWarnings("deprecation")
	public void printTradeReport(String fileName) throws IOException {

		ArrayList<TradeReportRequestDTO> tradeReportRequestDTOList = utilities.readInstructionFile(fileName);
		tradeReportRequestDTOList.forEach(p -> {

			Date currDate = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");

			try {
				currDate = sdf.parse(sdf.format(new Date()));
			} catch (ParseException pe) {
				pe.printStackTrace();
			}

			int day = currDate.getDay();
			if (p.getCurrency().equalsIgnoreCase("AED") || p.getCurrency().equalsIgnoreCase("SAR")) {

				if (day != 5 && day != 6) {

					if (day == 0) {

						if (p.getSettlementDate().equals(currDate) || p.getSettlementDate().equals(day - 1)
								|| p.getSettlementDate().equals(day - 2)) {
							if (p.getTradeIndicator().equalsIgnoreCase("B")) {
								buyAmount = buyAmount + getTotalTradeValue(p);// (a
																				// <
																				// b)
																				// ?
																				// a
																				// :
																				// b
								currencyBuyMap
										.put(p.getCurrency(),
												(currencyBuyMap.get(p.getCurrency()) == null ? 0.0
														: currencyBuyMap.get(p.getCurrency()))
														+ (getTotalTradeValue(p)));

							} else {
								sellAmoint = sellAmoint + getTotalTradeValue(p);
								currencySellMap
										.put(p.getCurrency(),
												(currencySellMap.get(p.getCurrency()) == null ? 0.0
														: currencySellMap.get(p.getCurrency()))
														+ (getTotalTradeValue(p)));
							}
						}

					} else if (p.getSettlementDate().equals(currDate)) {
						if (p.getTradeIndicator().equalsIgnoreCase("B")) {
							buyAmount = buyAmount + getTotalTradeValue(p);
							currencyBuyMap.put(p.getCurrency(), (currencyBuyMap.get(p.getCurrency()) == null ? 0.0
									: currencyBuyMap.get(p.getCurrency())) + (getTotalTradeValue(p)));
						} else {
							sellAmoint = sellAmoint + getTotalTradeValue(p);
							currencySellMap.put(p.getCurrency(), (currencySellMap.get(p.getCurrency()) == null ? 0.0
									: currencySellMap.get(p.getCurrency())) + (getTotalTradeValue(p)));
						}
					}
				}
			} else {
				if (day != 6 && day != 0) {

					if (day == 1) {
						if (p.getSettlementDate().equals(currDate)
								|| p.getSettlementDate().equals(getConvertedDate(currDate, -1))
								|| p.getSettlementDate().equals(getConvertedDate(currDate, -2))) {

							if (p.getTradeIndicator().equalsIgnoreCase("B")) {
								buyAmount = buyAmount + getTotalTradeValue(p);
								currencyBuyMap
										.put(p.getCurrency(),
												(currencyBuyMap.get(p.getCurrency()) == null ? 0.0
														: currencyBuyMap.get(p.getCurrency()))
														+ (getTotalTradeValue(p)));
							} else {
								sellAmoint = sellAmoint + getTotalTradeValue(p);
								currencySellMap
										.put(p.getCurrency(),
												(currencySellMap.get(p.getCurrency()) == null ? 0.0
														: currencySellMap.get(p.getCurrency()))
														+ (getTotalTradeValue(p)));
							}
						}
					} else if (p.getSettlementDate().equals(currDate)) {
						if (p.getTradeIndicator().equalsIgnoreCase("B")) {
							buyAmount = buyAmount + getTotalTradeValue(p);
							currencyBuyMap.put(p.getCurrency(), (currencyBuyMap.get(p.getCurrency()) == null ? 0.0
									: currencyBuyMap.get(p.getCurrency())) + (getTotalTradeValue(p)));
						} else {
							sellAmoint = sellAmoint + getTotalTradeValue(p);
							currencySellMap.put(p.getCurrency(), (currencySellMap.get(p.getCurrency()) == null ? 0.0
									: currencySellMap.get(p.getCurrency())) + (getTotalTradeValue(p)));
						}
					}
				}
			}

		});
	}
	
	/**
	 * printTradeReport
	 * 
	 * @param String fileName
	 */
	private Date getConvertedDate(Date date, int days) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.DATE, days);
		return c.getTime();

	}

	/**
	 * printTradeReport
	 * 
	 * @param String fileName
	 */
	private Double getTotalTradeValue(TradeReportRequestDTO tdo) {
		return tdo.getPricePerUnit() * tdo.getUnits() * tdo.getAgreedFx();
	}
}
