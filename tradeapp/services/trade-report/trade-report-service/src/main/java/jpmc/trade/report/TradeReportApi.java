package jpmc.trade.report;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;

/**
 * 
 * @author Prashant Goyal
 * class  TradeReportApi.
 *
 */
@SpringBootApplication
@RestController
public class TradeReportApi {

	/** The TradeReportService */
	@Autowired
	TradeReportService tradeReportService;

	/** The log */
	private static Logger log = Logger.getLogger(TradeReportApi.class);

	/**
	 * API method to call tradeReportService for printing daily report
	 * 
	 * @return Void
	 * @throws IOException
	 */

	@RequestMapping(value = "/report", method = RequestMethod.GET)
	public ResponseEntity<Void> printTradeReport() throws IOException {
		String fileName="E:\\software\\edinburgh\\workspace_jpmc\\tradeapp\\services\\trade-report\\trade-report-service\\src\\main\\resources\\inst.txt";
		tradeReportService.printTradeReport(fileName);
		return new ResponseEntity<>(HttpStatus.OK);

	}
	/*
	 * @RequestMapping(value = "/payment", method = RequestMethod.POST)
	 * 
	 * ResponseEntity<PaymentResponseDTO> payment(@RequestBody PaymentRequestDTO
	 * paymentRequestDTO) throws IOException {
	 * 
	 * log.debug("Calling payment method with PaymentRequestDTO {} ",
	 * paymentRequestDTO); if (paymentRequestDTO == null) { throw new
	 * InvalidInputException("Invalid Request", log); }
	 * 
	 * PaymentResponseDTO paymentResponseDTO =
	 * paymentService.payment(paymentRequestDTO);
	 * 
	 * if (paymentResponseDTO != null) { return
	 * ResponseEntity.ok(paymentResponseDTO); } else {
	 * 
	 * return new ResponseEntity<PaymentResponseDTO>(HttpStatus.NOT_FOUND); }
	 * 
	 * }
	 */

	public static void main(String[] args) throws Exception {
		log.info("Trade Report Service Starting ");
		SpringApplication.run(TradeReportApi.class, args);
	}

}
