package jpmc.trade.report;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

/**
 * 
 * @author Prashant Goyal
 * class  TradeReportConfiguration.
 *
 */
@Configuration
@Lazy
public class TradeReportConfiguration {
	
	/**
	 * TradeReportConfiguration
	 * 
	 * @return the Trade Report Service
	 */
	@Bean
	public TradeReportService tradeReportService(){
		return new TradeReportService();
	}

}
